use bookdata;
create table BookUsers(
username varchar(30) primary key  ,
password varchar(30)
);

create table Books(
id int primary key,
name varchar(30),
genre varchar(30)
);

insert into books values(101, "To Kill a Mockingbird", "Classics");
insert into books values(102, "Carrie",  "Horror");
insert into books values(103, "The Savior",  "Romance");
insert into books values(104, "Wings of Fire",  "Autobiography");
insert into books values(105, "Gandhi", "Autobiography");
insert into books values(106, "The Martian", "Sci-Fi");
insert into books values(107, "Animal Farm",  "Political Satire");
insert into books values(108, "Think and grow Rich",  "Self-Help");
insert into books values(109, "Can't hurt me", "Motivational");
insert into books values(110, "Getting things done",  "Self-Help");


create table ReadLater(
id int primary key,
name varchar(30),
genre varchar(30)
);


create table Likelist(
id int primary key,
name varchar(30),
genre varchar(30)
);